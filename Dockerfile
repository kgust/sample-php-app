FROM php:8.1

COPY app.php /opt/

ENTRYPOINT php -S 0:80 /opt/app.php
